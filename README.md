# README #

### What is this repository for? ###

* Simple single-page application homepage built with AngularJS, jQuery and HTML5/CSS3
* [View live](https://kim.roundeasy.ru)

### Requirements ###

* NodeJS
* SASS
* Grunt

### Installation ###
Project built with npm and grunt, so in project folder just:
```
#!bash
npm install
```
```
#!bash
grunt
```

### Usage ###
To live preview:
```
#!bash
grunt serve
```