(function () {
    'use strict';

    angular.module('roundeasyHome')

    .factory('Portfolio', ['$resource', 'baseURL', function ($resource, baseURL) {
        return $resource(baseURL + "/portfolio", null);
    }])

    .controller('portfolioController', ['$scope', 'Portfolio', function ($scope, Portfolio) {
        $scope.portfolio = [];
        Portfolio.query()
            .$promise.then(
                function (response) {
                    $scope.portfolio = response;
                    $scope.loading = false;
                },
                function (response) {
                    console.log('error');
                    $scope.loading = false;
                }
            );
    }]);
}());
