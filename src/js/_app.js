(function (angular) {
    'use strict';

    angular.module('roundeasyHome', ['ui.router', 'ngResource', 'views/portfolio.html', 'views/about.html'])

    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/about");
        //
        // Now set up the states
        $stateProvider
            .state('about', {
                url: "/about",
                templateUrl: "views/about.html",
                controller: 'aboutController'
            })
            .state('portfolio', {
                url: "/portfolio",
                templateUrl: "views/portfolio.html",
                controller: 'portfolioController'
            });
    }])

    .factory('Articles', ['$resource', 'baseURL', function ($resource, baseURL) {
        return $resource(baseURL + "/articles/:code", null, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });
    }])


    .directive('imageLoader', ['$timeout', function ($timeout) {
        return {
            restrict: 'AE',
            link: function (scope, element) {
                element.on("load", function () {
                    $(element).parent().addClass('loaded');
                    if ((element).data('mode') == 'background') {
                        var src = $(element).attr('src');
                        $(element).parent().css('background', 'transparent url("' + src + '") no-repeat scroll center center / cover');
                        $(element).remove();
                    }
                });
                element.on("error", function () {
                    $(element).parent().addClass('unloaded');
                });
            }
        };
    }])

    .directive('slickSlider', ['$timeout', function ($timeout) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {
                $timeout(function () {
                    $(element).slick({
                        dots: true,
                        speed: 500
                    });
                }, 1000);
            }
        };
    }]);

}(window.angular));
