(function() {
    'use strict';

    angular.module('roundeasyHome')

    .factory('Certificate', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL + "/certificates", null);
    }])

    .factory('Lifesteps', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL + "/lifesteps", null);
    }])

    .factory('Skills', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL + "/skills", null);
    }])

    .controller('aboutController', ['$scope', 'Certificate', 'Lifesteps', 'Skills', 'Articles',
        function($scope, Certificate, Lifesteps, Skills, Articles) {
            $scope.lifeline = [];
            $scope.experience = [];
            $scope.certificates = [];
            $scope.aboutArticle = [];
            $scope.contactArticle = [];

            $scope.showSkills = false;
            $scope.showTimeline = false;
            $scope.showCertificates = false;
            $scope.showArticle = false;
            $scope.showContact = false;


            Lifesteps.query()
                .$promise.then(
                    function(response) {
                        $scope.lifeline = response;
                        $scope.showTimeline = true;
                    },
                    function(response) {
                        console.log('error');
                    }
                );

            Skills.query()
                .$promise.then(
                    function(response) {
                        $scope.experience = response;
                        $scope.showSkills = true;
                    },
                    function(response) {
                        console.log('error');
                    }
                );

            Certificate.query()
                .$promise.then(
                    function(response) {
                        $scope.certificates = response;
                        $scope.showCertificates = true;
                    },
                    function(response) {
                        console.log('error');
                    }
                );

            Articles.get({ code: 'about' })
                .$promise.then(
                    function(response) {
                        $scope.aboutArticle = response[0];
                        $scope.showArticle = true;
                    },
                    function(response) {
                        console.log('error');
                    }
                );
            Articles.get({ code: 'contacts' })
                .$promise.then(
                    function(response) {
                        $scope.contactArticle = response[0];
                        $scope.showContact = true;
                    },
                    function(response) {
                        console.log('error');
                    }
                );
        }
    ])

    .filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
    }]);
}());
