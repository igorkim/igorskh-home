(function() {
    'use strict';

    module.exports = function(grunt) {

        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
            sass: { // Task
                dist: {
                    files: [{
                        expand: true,
                        cwd: 'src/sass',
                        src: ['*.scss'],
                        dest: 'app/dist/css',
                        ext: '.css'
                    }, {
                        expand: true,
                        cwd: 'src/sass',
                        src: ['*.scss'],
                        dest: 'app/dist/css',
                        ext: '.css'
                    }]
                }
            },
            jshint: {
                files: ['Gruntfile.js', 'src/js/*.js'],
                options: {
                    globals: {
                        jQuery: true
                    }
                }
            },
            connect: {
                test: {
                    options: {
                        port: 8000,
                        base: 'app/.',
                        livereload: true,
                        open: true
                    }
                }
            },
            uglify: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                },
                dist: {
                    src: ['src/js/*.js', '!src/js/_*.js'],
                    dest: 'app/dist/js/<%= pkg.name %>.min.js'
                }
            },
            watch: {
                scripts: {
                    files: ['src/js/*.js'],
                    tasks: ['jshint', 'copy'],
                    options: {
                        spawn: false,
                    },
                },
                css: {
                    files: 'src/sass/*.scss',
                    tasks: ['sass'],
                    options: {
                        livereload: true,
                    },
                },
                uglify: {
                    files: 'src/js/*.js',
                    tasks: ['uglify'],
                    options: {
                        livereload: true,
                    },
                },
                app: {
                    files: ['src/index.html', 'src/views/*.html'],
                    tasks: ['copy'],
                    options: {
                        livereload: true,
                    },
                }
            },
            copy: {
                js: {
                    files: [{
                        expand: true,
                        cwd: 'node_modules/jquery/dist',
                        src: 'jquery.min.js*',
                        dest: 'app/dist/js'
                    }, {
                        expand: true,
                        cwd: 'node_modules/slick-carousel/slick',
                        src: ['fonts/*', '*.min.js*', '*.css*', '*.gif'],
                        dest: 'app/dist/slick'
                    }, {
                        expand: true,
                        cwd: 'node_modules/angular',
                        src: 'angular.min.js*',
                        dest: 'app/dist/js'
                    }, {
                        expand: true,
                        cwd: 'node_modules/angular-ui-router/release',
                        src: 'angular-ui-router.min.js*',
                        dest: 'app/dist/js'
                    }, {
                        expand: true,
                        cwd: 'node_modules/angular-resource',
                        src: 'angular-resource.min.js*',
                        dest: 'app/dist/js'
                    }, {
                        expand: true,
                        cwd: 'src/js',
                        src: '_*.js',
                        dest: 'app/dist/js'
                    }, {
                        expand: true,
                        cwd: 'src',
                        src: ['index.html', 'favicon.ico'],
                        dest: 'app'
                    }, {
                        expand: true,
                        cwd: 'node_modules/material-design-lite',
                        src: ['*.min.js*', '*.min.css*'],
                        dest: 'app/dist/material-design-lite'
                    }]
                }
            },
            bump: {
                options: {
                    files: ['package.json'],
                    updateConfigs: [],
                    commit: true,
                    commitMessage: 'Release v%VERSION%',
                    commitFiles: ['package.json', 'app/**'],
                    createTag: true,
                    tagName: 'v%VERSION%',
                    tagMessage: 'Version %VERSION%',
                    push: true,
                    pushTo: 'origin',
                    gitDescribeOptions: "--tags --always --abbrev=1 --dirty=-d",
                    globalReplace: false,
                    prereleaseName: false,
                    metadata: '',
                    regExp: false
                }
            },
            html2js: {
                default: {
                    src: ['src/views/*.html'],
                    dest: 'app/dist/js/templates.js'
                }
            },
            cacheBust: {
                default: {
                    options: {
                        baseDir: './app/',
                        assets: ['dist/js/*.js', 'dist/css/*.css'],
                        queryString: true
                    },
                    src: ['app/index.html']
                }
            },
            clean: ['app/**']
        });

        grunt.loadNpmTasks('grunt-bump');
        grunt.loadNpmTasks('grunt-cache-bust');
        grunt.loadNpmTasks('grunt-contrib-sass');
        grunt.loadNpmTasks('grunt-contrib-connect');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-html2js');

        grunt.registerTask('build', ['default', 'bump']);
        grunt.registerTask('default', ['clean', 'jshint', 'uglify', 'copy', 'sass', 'html2js', 'cacheBust']);
        grunt.registerTask('serve', ['default', 'connect', 'watch']);
    };
}());
