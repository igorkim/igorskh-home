angular.module('templates-default', ['views/about.html', 'views/portfolio.html']);

angular.module("views/about.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("views/about.html",
    "<div class=\"mdl-grid\">\n" +
    "    <div class=\"article-card-wide-inline article-card-wide-about mdl-card mdl-shadow--2dp\">\n" +
    "        <div ng-hide=\"showArticle\" class=\"spinner\"></div>\n" +
    "        <div class=\"mdl-card__title\">\n" +
    "            <img data-mode=\"background\" image-loader ng-src=\"{{aboutArticle.imageUrl}}\">\n" +
    "            <div class=\"spinner\"></div>\n" +
    "            <div class=\"unfound\">the picture got lost on its way :-(</div>\n" +
    "            <h2 class=\"mdl-card__title-text\">{{aboutArticle.title}}</h2>\n" +
    "        </div>\n" +
    "        <div class=\"mdl-card__supporting-text\" ng-bind-html=\"aboutArticle.full | sanitize\" ng-show=\"showArticle\"></div>\n" +
    "    </div>\n" +
    "    <div class=\"article-card-wide-inline mdl-card mdl-shadow--2dp\">\n" +
    "        <div ng-hide=\"showContact\" class=\"spinner\"></div>\n" +
    "        <div class=\"mdl-card__title\">\n" +
    "            <h2 class=\"mdl-card__title-text\">{{contactArticle.title}}</h2>\n" +
    "        </div>\n" +
    "        <div class=\"mdl-card__supporting-text\" ng-bind-html=\"contactArticle.full | sanitize\" ng-show=\"showContact\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"article-card-wide-inline mdl-card mdl-shadow--2dp\">\n" +
    "        <div ng-hide=\"showSkills\" class=\"spinner\"></div>\n" +
    "        <div class=\"mdl-card__title\">\n" +
    "            <h2 class=\"mdl-card__title-text\">Skills & Experience</h2>\n" +
    "        </div>\n" +
    "        <div class=\"mdl-card__supporting-text timeline_horizontal\" ng-show=\"showSkills\">\n" +
    "            <ul class=\"timeline-elements\">\n" +
    "                <li ng-repeat=\"expItem in experience\">\n" +
    "                    <div class=\"label\">{{expItem.label}}</div>\n" +
    "                    <div class=\"inactive\" style=\"width:{{expItem.start}}%;\"></div>\n" +
    "                    <div ng-class=\"{end: expItem.isEnd, start: expItem.isStart}\" class=\"active\" style=\"width:{{expItem.end}}%;\"></div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <ul class=\"timeline-legend\">\n" +
    "                <li>2008</li>\n" +
    "                <li>2010</li>\n" +
    "                <li>2012</li>\n" +
    "                <li>2014</li>\n" +
    "                <li>2016</li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"article-card-wide-inline mdl-card mdl-shadow--2dp\">\n" +
    "        <div ng-hide=\"showTimeline\" class=\"spinner\"></div>\n" +
    "        <div class=\"mdl-card__title\">\n" +
    "            <h2 class=\"mdl-card__title-text\">Timeline</h2>\n" +
    "        </div>\n" +
    "        <div class=\"mdl-card__supporting-text timeline\" ng-show=\"showTimeline\">\n" +
    "            <li ng-repeat=\"lifeitem in lifeline\">\n" +
    "                <div class=\"border-line\"></div>\n" +
    "                <div class=\"timeline-description\">\n" +
    "                    <p>{{lifeitem.year}} - {{lifeitem.description}}, {{lifeitem.where}}</p>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "<!--     <div class=\"article-card-wide mdl-card mdl-shadow--2dp\">\n" +
    "        <div ng-hide=\"showCertificates\" class=\"spinner\"></div>\n" +
    "        <div class=\"mdl-card__title\">\n" +
    "            <h2 class=\"mdl-card__title-text\">Certificates</h2>\n" +
    "        </div>\n" +
    "        <div class=\"mdl-card__supporting-text slider\" slick-slider id=\"cert_slider\" ng-show=\"showCertificates\">\n" +
    "            <div ng-repeat=\"cert in certificates\" class=\"item\">\n" +
    "                <div class=\"label\">{{cert.label}}</div>\n" +
    "                <div class=\"placeholder\"></div>\n" +
    "                <div class=\"spinner\"></div>\n" +
    "                <img image-loader ng-src=\"{{cert.imageUrl}}\">\n" +
    "                <div class=\"label\">verify at <a target=\"_blank\" href=\"{{cert.verifyUrl}}\">{{cert.verifyLabel}}</a></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div> -->\n" +
    "</div>\n" +
    "");
}]);

angular.module("views/portfolio.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("views/portfolio.html",
    "<section class=\"portfolio\">\n" +
    "    <h2>Portfolio</h2>\n" +
    "    <p>Find more on <a target=\"_blank\" href=\"https://bitbucket.org/igorkim/\">bitbucket</a></p>\n" +
    "    <div class=\"mdl-grid\">\n" +
    "        <div class=\"portfolio-card-image mdl-card mdl-shadow--2dp\" ng-repeat=\"element in portfolio\">\n" +
    "            <img data-mode=\"background\" image-loader ng-src=\"{{element.imageUrl}}\">\n" +
    "            <div class=\"spinner\"></div>\n" +
    "            <div class=\"unfound\">the picture got lost on its way :-(</div>\n" +
    "            <div class=\"mdl-card__title mdl-card--expand\"></div>\n" +
    "            <div class=\"mdl-card__actions\">\n" +
    "                <p>\n" +
    "                    {{element.title}} - {{element.description}}\n" +
    "                    <a target=\"_blank\" ng-show=\"element.sourceUrl.length\" href=\"{{element.sourceUrl}}\"><span class=\"tag\">source</span></a>\n" +
    "                    <a target=\"_blank\" ng-show=\"element.url.length\" href=\"{{element.url}}\"><span class=\"tag\">live</span></a>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "");
}]);
